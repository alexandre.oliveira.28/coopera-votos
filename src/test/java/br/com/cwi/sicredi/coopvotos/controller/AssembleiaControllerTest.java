package br.com.cwi.sicredi.coopvotos.controller;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.time.LocalDate;

import org.junit.Test;

import br.com.cwi.sicredi.coopvotos.entities.Assembleia;

public class AssembleiaControllerTest {
	
	@Test(expected=IllegalArgumentException.class)
	public void naoCriarAssembleiaSemDescricao() throws Exception {
		new Assembleia(null, LocalDate.now());
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void naoCriarAssembleiaSemDataDaAssembleia() throws Exception {
		new Assembleia("Criação de assembleia", null);
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void dataNaoPodeSerMenorQueDataAtual() {
		new Assembleia("Criação de assembleia", LocalDate.of(2022,4,9));
	}
	
	@Test
	public void criarPautaComDadosPreenchidos() {
		Assembleia assembleia = new Assembleia("Criação de Assembleia", LocalDate.now());
		assertEquals("Criação de Assembleia", assembleia.getDescricao(), "Descrição devidamente preenchida");
		assertEquals(LocalDate.now(), assembleia.getDataAssembleia(), "Data da Assembleia devidamente preenchida");
	}


}
