package br.com.cwi.sicredi.coopvotos.controller;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.Test;

import br.com.cwi.sicredi.coopvotos.entities.Pauta;

public class PautaControllerTest {
	
	@Test(expected=IllegalArgumentException.class)
	public void naoCriarPautaSemDescricao() throws Exception {
		new Pauta(null, 1L);
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void naoCriarPautaSemIdAssembleia() {
		new Pauta("Criação de pauta", null);
	}
	
	@Test
	public void criarPautaComDadosPreenchidos() {
		Pauta p = new Pauta("Criação de pauta", 1L);
		assertEquals("Criação de pauta", p.getDescricao(), "Descrição devidamente preenchida");
		assertEquals(1L, p.getIdAssembleia(), "ID Assembleia devidamente preenchido");
	}

}
