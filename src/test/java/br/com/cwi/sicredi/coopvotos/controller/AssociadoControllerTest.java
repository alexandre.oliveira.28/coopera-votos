package br.com.cwi.sicredi.coopvotos.controller;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.Test;

import br.com.cwi.sicredi.coopvotos.entities.Associado;
import br.com.cwi.sicredi.coopvotos.utils.Utilitarios;

public class AssociadoControllerTest {
	
	String nomeAssociado = "Alexandre";
	String emailAssociado = "alexandre@gmail.com";
	String cpfAssociado = "00011122233";
	String cpfAssociadoValido = "23902294094";
	
	@Test(expected=IllegalArgumentException.class)
	public void naoCriarAssociadoSemNome() throws Exception {
		new Associado(null, emailAssociado, cpfAssociado);
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void naoCriarAssociadoSemEmail() {
		new Associado(nomeAssociado, null, cpfAssociado);
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void naoCriarAssociadoSemCpf() {
		new Associado(nomeAssociado, emailAssociado, null);
	}
	
	@Test
	public void criarPautaComDadosPreenchidos() {
		Associado associado = new Associado(nomeAssociado, emailAssociado, cpfAssociado);
		assertEquals(nomeAssociado, associado.getNome(), "Nome devidamente preenchido");
		assertEquals(emailAssociado, associado.getEmail(), "Email devidamente preenchido");
	}
	
	@Test
	public void verificaTamanhoCpf() {
		assertEquals(11, cpfAssociado.length(), "CPF com tamanho correto");
	}
	
	@Test
	public void verificaCpfInvalido() {
		assertEquals(false, Utilitarios.validaCpf(cpfAssociado), "CPF Inválido");
	}
	
	@Test
	public void verificaCpfValido() {
		assertEquals(true, Utilitarios.validaCpf(cpfAssociadoValido), "CPF Válido");
	}
	
}
