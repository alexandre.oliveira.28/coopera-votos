package br.com.cwi.sicredi.coopvotos.controller;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.time.LocalDateTime;

import org.junit.Test;

import br.com.cwi.sicredi.coopvotos.entities.SessaoVotacao;

public class SessaoVotacaoControllerTest {
	
	String descricao = "Abertura de Sessão";
	Long idPauta = 1L;
	Integer tempoAbertura = 60;
	LocalDateTime encerramentoSessao = LocalDateTime.of(2022, 6, 14, 16, 0);
	LocalDateTime encerramentoDepois = LocalDateTime.now().plusHours(2);

	@Test(expected=IllegalArgumentException.class)
	public void naoCriarSessaoSemDescricao() throws Exception {
		new SessaoVotacao(null, idPauta, tempoAbertura);
	}

	@Test(expected=IllegalArgumentException.class)
	public void naoCriarSessaoSemPauta() throws Exception {
		new SessaoVotacao(descricao, null, tempoAbertura);
	}

	@Test
	public void criarSessaoComTempoDeAberturaNula() throws Exception {
		new SessaoVotacao(descricao, idPauta, null);
	}
	
	@Test
	public void naoPermitirVotacaoAposEncerramentoDaSessao() {
		assertEquals(true, encerramentoSessao.isBefore(LocalDateTime.now()), "Votação não permitida. Tempo esgotado!");
	}
	
	@Test
	public void permitirVotacaoCasoDataHoraSejaMaiorQueAgora() {
		assertEquals(true, encerramentoDepois.isAfter(LocalDateTime.now()), "Votação permitida. Dentro do prazo!");
	}

}
