package br.com.cwi.sicredi.coopvotos.controller;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.time.LocalDateTime;

import org.junit.Before;
import org.junit.Test;

import br.com.cwi.sicredi.coopvotos.entities.Associado;
import br.com.cwi.sicredi.coopvotos.entities.SessaoVotacaoAssociado;

public class SessaoVotacaoAssociadoControllerTest {
	
	Long idSessaoVotacao = 1L;
	Long idAssociado = 1L;
	Boolean voto = true;
	String nome = "Alexandre";
	String email = "alexandre@gmail.com";
	String cpf = "23902294094";
	Boolean apto = true;
	Associado associado = new Associado();
	
	@Before
	public void setup() {
		associado.setNome(nome);
		associado.setEmail(email);
		associado.setCpf(cpf);
		associado.setApto(apto);
		associado.setDataCadastro(LocalDateTime.now());
		
	}

	@Test(expected=IllegalArgumentException.class)
	public void naoVotarSemInformarAberturaDeSessao() throws Exception {
		new SessaoVotacaoAssociado(null, idAssociado, voto);
	}

	@Test(expected=IllegalArgumentException.class)
	public void naoPermitirVotoSemAssociado() throws Exception {
		new SessaoVotacaoAssociado(idSessaoVotacao, null, voto);
	}

	@Test(expected=IllegalArgumentException.class)
	public void naoPermitirVotoNull() throws Exception {
		new SessaoVotacaoAssociado(idSessaoVotacao, idAssociado, null);
	}
	
	@Test
	public void verificaAptdaoAssociado() {
		assertEquals(true, associado.getApto(), "Apto para votar");
	}

}
