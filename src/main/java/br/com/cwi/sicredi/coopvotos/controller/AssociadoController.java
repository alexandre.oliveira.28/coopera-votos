package br.com.cwi.sicredi.coopvotos.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.cwi.sicredi.coopvotos.entities.Associado;
import br.com.cwi.sicredi.coopvotos.service.AssociadoService;

@RestController
@RequestMapping("/v1/associado")
public class AssociadoController {
	
	@Autowired
	private AssociadoService associadoService;
	
	@PostMapping("/cadastrar")
	public ResponseEntity<Associado> salvarAssociado(String nome, String email, String cpf, Boolean apto) {
		associadoService.salvarAssociado(nome, email, cpf, apto);
		return ResponseEntity.ok().build();
	}
	
	@GetMapping("/{cpf}")
	public String verificaAssociado(@PathVariable("cpf") String cpf) {
		return associadoService.verificaAssociado(cpf);
	}

}
