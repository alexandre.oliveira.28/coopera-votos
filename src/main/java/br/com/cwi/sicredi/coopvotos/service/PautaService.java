package br.com.cwi.sicredi.coopvotos.service;

import java.time.LocalDateTime;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.cwi.sicredi.coopvotos.entities.Pauta;
import br.com.cwi.sicredi.coopvotos.repository.PautaRepository;

@Service
public class PautaService {
	
	private static final Logger log = LoggerFactory.getLogger (PautaService.class);
	
	@Autowired
	private PautaRepository pautaRepository;

	public void salvarPauta(String descricao, Long idAssembleia) {
		
		Pauta pauta = new Pauta();
		pauta.setDescricao(descricao);
		pauta.setIdAssembleia(idAssembleia);
		pauta.setDataCadastro(LocalDateTime.now());
		
		pautaRepository.save(pauta);
		
		log.info("Cadastrando Pauta -> [ID "+pauta.getId()+"]");
	}
	
}