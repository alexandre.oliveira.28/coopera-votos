package br.com.cwi.sicredi.coopvotos.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.cwi.sicredi.coopvotos.entities.SessaoVotacao;
import br.com.cwi.sicredi.coopvotos.service.SessaoVotacaoService;

@RestController
@RequestMapping("/v1/sessao-votacao")
public class SessaoVotacaoController {
	
	@Autowired
	private SessaoVotacaoService sessaoVotacaoService;
	
	@PostMapping("/abrir")
	public ResponseEntity<SessaoVotacao> abrirSessaoVotacao(String descricao, Long idPauta, Integer tempoAbertura) {
		sessaoVotacaoService.abrirSessaoVotacao(descricao, idPauta, tempoAbertura);
		return ResponseEntity.ok().build();
	}

}
