package br.com.cwi.sicredi.coopvotos.entities;

import java.time.LocalDate;
import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

@Entity
@Table(name = "assembleia")
@Data
public class Assembleia {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_assembleia")
	private Long id;

	@Column(nullable = false)
	private String descricao;
	
	@Column(name = "data_assembleia", nullable = false)
	private LocalDate dataAssembleia;
	
	@Column(name = "data_cadastro", nullable = false)
	private LocalDateTime dataCadastro;

	
	public Assembleia() {
	}
	
	public Assembleia(String descricao) {
		this.descricao = descricao;
	}

	public Assembleia(String descricao, LocalDate dataAssembleia) {
		
		if( descricao == null){
			throw new IllegalArgumentException("A Assembiela não pode ser criada sem uma DESCRIÇÃO.");
		}
		
		if(dataAssembleia == null ){
			throw new IllegalArgumentException("A Assembiela não pode ser criada sem uma DATA.");
		}
		
		if(dataAssembleia.isBefore(LocalDate.now())) {
			throw new IllegalArgumentException("Data da Assembleia não pode ser menor que data atual.");
		}
		
		this.descricao = descricao;
		this.dataAssembleia = dataAssembleia;
	}
	
}
