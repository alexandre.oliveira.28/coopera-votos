package br.com.cwi.sicredi.coopvotos.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.cwi.sicredi.coopvotos.entities.Pauta;

public interface PautaRepository extends JpaRepository<Pauta, Long> {

}
