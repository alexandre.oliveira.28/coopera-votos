package br.com.cwi.sicredi.coopvotos.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.cwi.sicredi.coopvotos.entities.SessaoVotacaoAssociado;

public interface SessaoVotacaoAssociadoRepository extends JpaRepository<SessaoVotacaoAssociado, Long> {
	
	Optional<SessaoVotacaoAssociado> findByIdSessaoVotacaoAndIdAssociado(Long idSessaoVotacao, Long idAssembleia);

}
