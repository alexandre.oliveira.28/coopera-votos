package br.com.cwi.sicredi.coopvotos.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.cwi.sicredi.coopvotos.entities.SessaoVotacao;
import br.com.cwi.sicredi.coopvotos.service.SessaoVotacaoAssociadoService;

@RestController
@RequestMapping("/v1/sessao-votacao-associado")
public class SessaoVotacaoAssociadoController {
	
	@Autowired
	private SessaoVotacaoAssociadoService sessaoVotacaoAssociadoService;
	
	@PostMapping("/votar")
	public ResponseEntity<SessaoVotacao> enviarVoto(Long idAssociado, Long idSessaoVotacao, Boolean voto) {
		sessaoVotacaoAssociadoService.enviarVoto(idAssociado, idSessaoVotacao, voto);
		return ResponseEntity.ok().build();
	}

}
