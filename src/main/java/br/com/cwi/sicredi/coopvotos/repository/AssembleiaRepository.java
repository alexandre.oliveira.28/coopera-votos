package br.com.cwi.sicredi.coopvotos.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.cwi.sicredi.coopvotos.entities.Assembleia;

public interface AssembleiaRepository extends JpaRepository<Assembleia, Long> {

	Optional<Assembleia> findById(Long idAssembleia);
	
}
