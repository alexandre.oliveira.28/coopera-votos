package br.com.cwi.sicredi.coopvotos.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.cwi.sicredi.coopvotos.entities.Pauta;
import br.com.cwi.sicredi.coopvotos.service.PautaService;

@RestController
@RequestMapping("/v1/pauta")
public class PautaController {
	
	@Autowired
	private PautaService pautaService;
	
	@PostMapping("/cadastrar")
	public ResponseEntity<Pauta> salvarPauta(String descricao, Long idAssembleia) {
		pautaService.salvarPauta(descricao, idAssembleia);
		return ResponseEntity.ok().build();
	}
	
	@GetMapping("/teste")
	public String teste() {
		return "Testando Endpoint";
	}

}
