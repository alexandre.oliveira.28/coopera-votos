package br.com.cwi.sicredi.coopvotos.service;

import java.time.LocalDateTime;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.cwi.sicredi.coopvotos.entities.Associado;
import br.com.cwi.sicredi.coopvotos.repository.AssociadoRepository;
import br.com.cwi.sicredi.coopvotos.utils.Utilitarios;

@Service
public class AssociadoService {
	
	private static final Logger log = LoggerFactory.getLogger (AssociadoService.class);
	
	@Autowired
	private AssociadoRepository associadoRepository;

	public void salvarAssociado(String nome, String email, String cpf, Boolean apto) {
		
		Associado associado = new Associado();
		associado.setNome(nome);
		associado.setEmail(email);
		associado.setCpf(cpf);
		associado.setApto(apto);
		associado.setDataCadastro(LocalDateTime.now());
		
		associadoRepository.save(associado);
		
		log.info("Cadastrando Associado -> [ID "+associado.getId()+"]");
	}
	
	public String verificaAssociado(String cpf) {
		
		Associado associado = associadoRepository.findByCpf(cpf);
		StringBuilder resultado = new StringBuilder();
		
		if(!Utilitarios.validaCpf(cpf)) {
			resultado.append("CPF inválido! \nPor favor, digite corretamente.");
		}else if(associado == null) {
			resultado.append("Associado não encontrado na nossa base de dados.");
		}else {
			String simNao = associado.getApto() == true ? "Sim" : "Não";		
			resultado.append("O Associado (" + associado.getNome() + ") pode votar: " + simNao);			
		}
		
		return resultado.toString();
	}
	
}