package br.com.cwi.sicredi.coopvotos.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.cwi.sicredi.coopvotos.entities.SessaoVotacao;

public interface SessaoVotacaoRepository extends JpaRepository<SessaoVotacao, Long> {
	
	SessaoVotacao findById(Integer idSessaoVotacao);

}
