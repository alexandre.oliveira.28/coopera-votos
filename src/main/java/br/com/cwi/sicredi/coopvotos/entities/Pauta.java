package br.com.cwi.sicredi.coopvotos.entities;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

@Entity
@Table(name = "pauta")
@Data
public class Pauta {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_pauta")
	private Long id;

	@Column(nullable = false)
	private String descricao;
	
	@Column(name = "data_cadastro")
	private LocalDateTime dataCadastro;
	
	@Column(name = "id_assembleia")
	private Long idAssembleia;
			
	public Pauta() {
		super();
	}

	public Pauta(String descricao, Long idAssembleia) {
		
		if( descricao == null){
			throw new IllegalArgumentException("A descrição da Pauta não pode ser nula ou vazia");
		}
		
		if(idAssembleia == null ){
			throw new IllegalArgumentException("Deve ser informada uma Assembleia para a Pauta");
		}
		
		this.dataCadastro = LocalDateTime.now();
		this.descricao = descricao;
		this.idAssembleia = idAssembleia;
	}
	

}
