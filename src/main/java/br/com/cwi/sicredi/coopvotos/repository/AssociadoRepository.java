package br.com.cwi.sicredi.coopvotos.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.cwi.sicredi.coopvotos.entities.Associado;

public interface AssociadoRepository extends JpaRepository<Associado, Long> {
	
	Associado findById(Integer idAssociado);
	
	Associado findByCpf(String cpf);

}
