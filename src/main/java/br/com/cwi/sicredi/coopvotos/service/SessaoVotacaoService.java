package br.com.cwi.sicredi.coopvotos.service;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.cwi.sicredi.coopvotos.entities.SessaoVotacao;
import br.com.cwi.sicredi.coopvotos.repository.SessaoVotacaoRepository;

@Service
public class SessaoVotacaoService {
	
	private static final Logger log = LoggerFactory.getLogger (SessaoVotacaoService.class);
	
	@Autowired
	private SessaoVotacaoRepository sessaoVotacaoRepository;

	public StringBuilder abrirSessaoVotacao(String descricao, Long idPauta, Integer tempoAbertura) {
		
		SessaoVotacao s = new SessaoVotacao();
		s.setDescricao(descricao);
		s.setIdPauta(idPauta);
		s.setTempoAbertura(tempoAbertura);
		s.setDataCadastro(LocalDateTime.now());
		
		/*
		 * Caso não seja informado o tempo de abertura ou informado ZERO, será setado 1 minuto por default 
		 */
		if(s.getTempoAbertura() == null || s.getTempoAbertura() == 0) {
			s.setTempoAbertura(1);
		}
		
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm");

        String inicioSessao = LocalDateTime.now().format(formatter);
        String encerramentoSessao = LocalDateTime.now().plusMinutes(s.getTempoAbertura()).format(formatter);
		
		StringBuilder sb = new StringBuilder();
		sb.append("Início da Sessão: " + inicioSessao + " ---- ");
		sb.append("Encerramento da Sesão: " + encerramentoSessao);
		
		sessaoVotacaoRepository.save(s);
		
		log.info("Abertura de Sessão -> [ID "+s.getId()+"]");
		log.info(sb.toString());

		return sb;
		
	}
	
}