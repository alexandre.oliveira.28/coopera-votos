package br.com.cwi.sicredi.coopvotos.service;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.cwi.sicredi.coopvotos.entities.Assembleia;
import br.com.cwi.sicredi.coopvotos.repository.AssembleiaRepository;

@Service
public class AssembleiaService {
	
	private static final Logger log = LoggerFactory.getLogger (AssembleiaService.class);
	
	@Autowired
	private AssembleiaRepository assembleiaRepository;

	public void salvarAssembleia(String descricao, String dataAssembleia) {
		
	    DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");
	    LocalDate data = LocalDate.parse(dataAssembleia,formatter);
		
		Assembleia assembleia = new Assembleia();
		assembleia.setDescricao(descricao);
		assembleia.setDataAssembleia(data);
		assembleia.setDataCadastro(LocalDateTime.now());
		
		assembleiaRepository.save(assembleia);
		
		log.info("Cadastrando Assembleia -> [ID "+assembleia.getId()+"]");
	}

}