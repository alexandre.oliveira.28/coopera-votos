package br.com.cwi.sicredi.coopvotos.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.cwi.sicredi.coopvotos.entities.Assembleia;
import br.com.cwi.sicredi.coopvotos.service.AssembleiaService;

@RestController
@RequestMapping("/v1/assembleia")
public class AssembleiaController {
	
	@Autowired
	private AssembleiaService assembleiaService;
	
	@PostMapping("/cadastrar")
	public ResponseEntity<Assembleia> salvarAssembleia(String descricao, String dataAssembleia) {
		assembleiaService.salvarAssembleia(descricao, dataAssembleia);
		return ResponseEntity.ok().build();
	}

}
