package br.com.cwi.sicredi.coopvotos;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan(basePackageClasses = {CoopvotosApplication.class})
public class CoopvotosApplication {

	public static void main(String[] args) {
		SpringApplication.run(CoopvotosApplication.class, args);
	}

}
