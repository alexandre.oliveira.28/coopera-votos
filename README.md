# Coopera Votos SICREDI

No cooperativismo, cada associado possui um voto e as decisões são tomadas em assembleias, por votação.
A partir disso, você precisa criar uma solução back-end para gerenciar essas sessões de votação.
Essa solução deve ser executada na nuvem

## Promover as seguintes funcionalidades através de uma API REST:
- Cadastrar uma nova pauta;
- Abrir uma sessão de votação em uma pauta (a sessão de votação deve ficar aberta por um tempo
determinado na chamada de abertura ou 1 minuto por default);
- Receber votos dos associados em pautas (os votos são apenas 'Sim'/'Não'. Cada associado é
identificado por um id único e pode votar apenas uma vez por pauta);
- Contabilizar os votos e dar o resultado da votação na pauta.

## Endpoints criados

- [1] [POST /pauta/cadastrar](http://127.0.0.1:8080/v1/pauta/cadastrar)
- [2] [POST /sessao-votacao/abrir](http://127.0.0.1:8080/v1/sessao-votacao/abrir)
- [3] [POST /associado/cadastrar](http://127.0.0.1:8080/v1/associado/cadastrar)
- [4] [POST /sessao-votacao-associado/votar](http://127.0.0.1:8080/v1/sessao-votacao-associado/votar)
- [5] [GET /v1/associado/03099988801](http://127.0.0.1:8080/v1/associado/03099988801)

***

# Softwares Utilizados

- [ ] [PostgreSql 9.6](https://www.enterprisedb.com/postgresql-tutorial-resources-training?uuid=91f8264f-8a2d-434a-9559-4dfa9b3b80b1&campaignId=701380000017oAXAAY)
- [ ] [pgAdmin4 6.10](https://www.postgresql.org/ftp/pgadmin/pgadmin4/v6.10/windows/)
- [ ] [Eclipse - 2019-09 R (4.13.0)](https://www.eclipse.org/downloads/download.php?file=/technology/epp/downloads/release/2019-09/R/eclipse-jee-2019-09-R-win32-x86_64.zip)
- [ ] [Postman](https://dl.pstmn.io/download/latest/win64)
- [ ] [Spring Boot v2.7.0](https://spring.io/projects/spring-boot)

***

# Tarefa Bônus 1 - Integração com sistemas externos
- Foi utilizado o endpoint 5 para realizar esta tarefa

***

# Tarefa Bônus 2 - Mensageria e filas
- lib-rabbitmq: Biblioteca para inserir classes que serão utilizadas pelos microsserviços e aplicação principal.
- mensageria-sicredi: API para enviar as mensagens aos consumidores.
- resultado-consumer: Microsserviço que receberá as mensagens pertinentes aos resultados das votações.

***

# Tarefa Bônus 4 - Versionamento da API
- Utilizando o padrão *"/v1"* nas URLs das APIs para especificar a versão utilizada nos endpoints é uma estratégia que pode evitar problemas de incompatibilidades em caso de mudanças no sistema

***